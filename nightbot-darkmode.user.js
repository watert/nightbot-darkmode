// ==UserScript==
// @name         Nightbot-Darkmode
// @namespace    https://gitlab.com/watert/nightbot-darkmode
// @version      0.1.1
// @description  Simple styler module for dark mode of nightbot.tv
// @supportURL   https://gitlab.com/watert/nightbot-darkmode/-/issues
// @downloadURL  https://gitlab.com/watert/nightbot-darkmode/-/raw/main/nightbot-darkmode.user.js
// @updateURL    https://gitlab.com/watert/nightbot-darkmode/-/raw/main/nightbot-darkmode.user.js
// @author       Mr.WaterT
// @match        https://nightbot.tv/*
// @icon         https://nightbot.tv/favicon.ico
// @grant        GM_addStyle
// @run-at       document-end
// ==/UserScript==

(function() {
    GM_addStyle(`
* {
    --c-dark: #1e282e; /*#2d3c45;*/
    --c-white: white;
    --c-border: #add8e6;
    --c-link: #add8e6;
    --c-disabled: #a5a9ab;
}

#page-wrapper,
ul.dropdown-menu {
    background: var(--c-dark);
    color: white;
}

#page-wrapper>*>*>*,
#page-wrapper>*>*>*>*,
#page-wrapper>*>*>*>*>*,
#page-wrapper>*>*>*>*>*>*>*,
#page-wrapper tr,
#page-wrapper th,
#page-wrapper select,
#page-wrapper input,
#page-wrapper a,
#page-wrapper .footer,
.white-bg,
.ibox *
{ background: transparent; }

input::placeholder,
i.fa,
[ng-repeat*=playlist],
#page-wrapper .disabled a
{ color: var(--c-disabled) !important; }

a { color: var(--c-link); }
button.btn,
a.btn { border-color: var(--c-border); }
.ibox-title { border-top-width: 1px; }
.slider-selection { background: var(--c-white); }
.slider-track {
    background: var(--c-dark);
    border: 1px solid var(--c-border);
}

::-webkit-scrollbar { width:20px; }
::-webkit-scrollbar-thumb {
    background:#5f6368;
    background-clip:padding-box;
    border:2px solid transparent;
    box-shadow:none;
    min-height:50px;
}
::-webkit-scrollbar-thumb:hover {
    background-color: var(--c-disabled);
}
::-webkit-scrollbar-thumb:active {
    background-color: var(--c-link);
}
[config=statsGraph] svg {
    -webkit-filter: invert(100%);
}`
    );
})();
