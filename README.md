# Nightbot-Darkmode

[Nightbot](https://nightbot.tv)의 관리 페이지를 다크 테마로 사용하기 위한 간단한 [Tampermoneky](https://www.tampermonkey.net)용 스크립트입니다.

탬퍼몽키를 사용하고 있는 환경에서 [링크](https://gitlab.com/watert/nightbot-darkmode/-/raw/main/nightbot-darkmode.user.js)를 클릭하여 설치합니다.

---

현재 Edge 브라우저에서 테스트 됨.
